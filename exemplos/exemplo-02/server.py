from flask import Flask

app = Flask(__name__)

@app.route('/')
def main():
    return 'aqui'

if __name__ == '__main__':
    app.run()