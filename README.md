# FLASK


# OBJETIVO

Ensinar como começar um projeto com flask, como utilizar os parâmetros de `header`, `body`, `params` e `query params` bem como o sistema de rotas, e por fim como utilizar o flask com uwsgi e docker.

> OBS.: Utilizarei o flask como endpoint de api, então não vou trabalhar com a parte de tamplates e jinja


# Como criar uma aplicação flask

Para começar a utilizar o flask, você deve primeiro instalar o mesmo, para isso basta digitar o comando

```sh
$ pip install flask
```

Para criar qualquer aplicação flask, você vai precisar de ao menos três linhas de código, um import do flask, a criação do objeto `app` que irá representar toda a aplicação e mandar a app ser executada com run, logo a aplicação base fica da seguinte forma:

```py
from flask import Flask

app = Flask(__name__)

app.run()
```

crie um arquivo com essas três linhas e depois execute

```
$ python <arquivo.py>
```

o flask irá ter criado um server na porta 5000, você pode acessar pelo navegador, mas verá um not found, pois ainda não temos nenhuma rota para a aplciação


# Rotas

As rotas no flask são definidas utilizando decorators em funções, sendo que essas funções serão executadas assim que a rota for chamada. O decorator em questão vem justamente da `app` que irá fazer a verificação de quem deve chamar de acordo com a rota. Então vamos fazer a rota `/` retornando apenas um "oi"


```py
from flask import Flask

app = Flask(__name__)

@app.route('/')
def api():
    return 'oi'

app.run()
```

Agora no seu navegador já é possivel ver algo quando bate na url `localhost:5000`.

Agora vou criar mais uma, a rota `/api`

```py
from flask import Flask

app = Flask(__name__)

@app.route('/')
def api():
    return 'oi'

@app.route('/api')
def api():
    return 'oi da api'

app.run()
```

Reinicie o servidor e bata na url `localhost:5000/api` que vai ver que veio a nova mensagem


# Reload automático

Do jeito que está agora, caso você modifique algo no seu arquivo e salve, o servidor não irá dar reload automáticamente, você precisa ficar desligando ele e executar novamente para aplicar cada mudança. Mas para o desenvolvimente isso não é muito bom, é melhor salvar e já recarregar tudo para que seja tudo mais rápido. Para isso é só informar o flask que você está em modo de desenvolvimento. E para tal basta definir a variável ambiente `FLASK_ENV = development` e pronto, você pode fazer isso no terminal antes de chamar o server, mas fica mais fácil colocar isso no proprio código

```py
from flask import Flask
import os

app = Flask(__name__)

@app.route('/')
def api():
    return 'oi'


os.environ['FLASK_ENV'] = 'development'
app.run()
```

Agora faça o teste, acesse `localhost:5000` e depois modifique o arquivo do server (a resposta do return) e salve com o server rodando, você pode agora apenas recarregar a página do browser que a mudança já estará lá.

# Retornar json

Para retornar json, no esquema de api, é bem simples, basta importar o jsonnify do flask e passar um dicionário

```py
from flask import Flask, request, jsonify
import os

app = Flask(__name__)

@app.route('/')
def testParams():
    return jsonify({
        'msg': 'oi'
    })

os.environ['FLASK_ENV'] = 'development'
app.run()
```


# Parâmetros

## params

Se quiser passar parâmetros via url é bem simples, basta definir o nome dos parâmetros entre `<>` na rota e pronto. Para usá-los passe-os como atributos da função da rota.

```py
from flask import Flask, request, jsonify
import os

app = Flask(__name__)

@app.route('/<param_value>')
def testParams(param_value):
    return jsonify({
        'msg': param_value
    })

os.environ['FLASK_ENV'] = 'development'
app.run()
```

agora coloque a url `localhost:5000/teste` que você verá o parâmetro `teste` na resposta do json


## query params

Se quiser receber um parâmetro dessa forma, basta usar o `request.args.get('nome_do_parâmetro')` dentro da função da rota, a rota em sí não precisa receber nenhum parâmetro

```py
from flask import Flask, request, jsonify
import os

app = Flask(__name__)

@app.route('/')
def testQueryPamans():
    param_value = request.args.get('param_value')
    return jsonify({
        'msg': param_value
    })

os.environ['FLASK_ENV'] = 'development'
app.run()
```

agora coloque a url `localhost:5000?param_value=teste` que você verá o parâmetro `teste` na resposta do json



# header

Assim como os query params os parâmetros vindos do header não gera a necessidade de modificar a descrição da rota. E para usá-los dentro da função basta utilizar `request.headers['nome_do_parâmetro']`.

```py
from flask import Flask, request, jsonify
import os

app = Flask(__name__)

@app.route('/')
def testHeader():
    param_value = request.headers['param_value']
    return jsonify({
        'msg': param_value
    })

os.environ['FLASK_ENV'] = 'development'
app.run()
```

para testar esse você vai precisar de um client http, pois vai precisar passar o header, para ficar mais universal eu vou escrever a request no curl

```sh
$ curl --header "param_value: ola" localhost:5000
```

# body

Para coletar os valores do body, utilize `request.body[nome_do_parametro]`. Mas também pode utilizar `request.json[nome_do_parametro]` caso saiba que irá chegar um json, assim ele já converte para um dict. E mais importante, é permitir o método de POST, pois até agora só o método de GET está sendo permitido (default), para isso basta fazer uma pequena mudança no decorator

```py
from flask import Flask, request, jsonify
import os

app = Flask(__name__)

@app.route('/', methods=['POST'])
def testBody():
    print(request.json)
    param_value = request.json
    return jsonify({
        'msg': param_value
    })

os.environ['FLASK_ENV'] = 'development'
app.run()
```


```sh
$ curl --header "Content-Type: application/json" \
       -pequest POST \
       --data '{"campo_01": "valor_01", "campo_02": "valor_02"}' \
       http://localhost:5000
```

# Métodos HTTP

Como viu anteriormente, o flask permite por padrão apenas GET, mas é possível permitir o tipo de método específico ou multiplos métodos por rota, somente modificando o decorator.

```py
@app.route('/', methods=['GET', 'POST', 'PUT', 'DELETE'])
```

# Deploy
Rode o servidor sem passar a variável de ambiente. você verá que o flask retorna a seguinte mensagem:

```sh
WARNING: This is a development server. Do not use it in a production deployment.
Use a production WSGI server instead.
```

Isso porque o flask cria um servidor muito simples, que não é ideal para ambientes de produção. Logo precisa-se de um servidor WSGI para isso. Existem várias formas de criar um, que se econtram na própria [documentação do flask](https://flask.palletsprojects.com/en/2.0.x/deploying/). Porém, um bem simples e muito bom é o uWSGI.

Vou fazer o exemplo passo a passo aqui, mas vou deixar todos os aquivos na pasta do [exemplo-01](./exemplos/exemplo-01)

- 1) Primeiro você deve ter o código flask, mas dessa vez com a sessão de `if __name__ == '__main__':` definida para rodar a app, pois agora não vamos mais precisar mandar rodar a app, então devemos separá-la (se não sabe o que é isso, faça uma pesquisa rapida que vai achar). Então o server bem simples fica:

```py
from flask import Flask

app = Flask(__name__)

@app.route('/')
def main():
    return 'aqui'

if __name__ == '__main__':
    app.run()
```

- 2) agora você precisa instalar o uwsgi
```py
$ pip install uwsgi
```

- 3) crie o arquivo de configuração do uwsgi chamado `app.ini` junto ao arquivo do servidor python

 ```ini
[uwsgi]
# arquivo server.py chamando a variável app (app.run())
wsgi-file = server.py
callable = app

# modo mestre com 4 processos em paralelo
master = true
processes = 4

# porta que o socket funcionará
http = :5000
```

- 4) execute o server (o terminal deve estar no mesmo local que o arquivo app.ini)

```
$ uwsgi app.ini
```

E agora pode ir no browser em `localhost:5000` que vai estar com o server rodando por lá. 

## Deploy com Docker e Nginx
O uWSGI sozinho é bom, mas esse não é o melhor modo, o melhor mesmo é colocando o `nginx` na frente e o protocolo de comunicação entre o nginx e o uWSGI ser socket e não http, pois é mais rapido, e então nos faremos a comunicação http com o nginx. E para isso eu vou usar docker.


Esse caso eu vou criar dentro de [exemplo-02](./exemplos/exemplo-02)

Para isso vamos ter alguns arquivos importantes aqui

- `Dockerfile`
    - vou utilizar ele para definir uma imagem docker que tenha o nginx, flask, uwsgi e toda a aplicação dentro.
    ```yml
        # imagem do nginx
        FROM nginx:latest

        # remover a configuração default do nginx e colocar a minha
        RUN rm /etc/nginx/conf.d/default.conf
        COPY exemplo_flask.conf /etc/nginx/conf.d/exemplo_flask.conf

        # instalar o python
        RUN apt update && apt upgrade -y
        RUN apt install python3 -y
        RUN apt install python3-pip -y

        # copiar todos os arquivos do python para o workdir em /app
        WORKDIR /app
        COPY start-service.sh .
        COPY server.py server.py
        COPY app.ini app.ini
        COPY requirements.txt requirements.txt

        # instalar os requirements (flask e uwsgi)
        RUN pip3 install -r requirements.txt

        # dar permissão de execução no scirpt `start-server.sh` que irá 
        # simplesmente conter o comando para chamar o uwsgi e o nginx
        RUN chmod +x start-service.sh

        # export as portas e colocar o comando e inicialização chamando o script
        EXPOSE 80
        EXPOSE 5000

        CMD ["./start-service.sh"]
    ```

- server.py
    - aqui nada de mais, só o server flask mesmo
    ```py
        from flask import Flask

        app = Flask(__name__)

        @app.route('/')
        def main():
            return 'aqui'

        if __name__ == '__main__':
            app.run()
    ```

- app.ini
    - O arquivo de configurações do uwsgi mudou bem pouco, agora ele apenas indica que terá um socket no lugar do http
    ```ini
        [uwsgi]
        # arquivo main.py chamando a variável app (app.run())
        wsgi-file = /app/server.py
        callable = app

        # modo mestre com 4 processos em paralelo
        master = true
        processes = 4

        # executar no modo de socket
        socket = exemplo_flask.sock
        chmod-socket = 777

        # limpar o socket quando o processo parar
        vacuum = true

        # alinha o uwsgi com o system init
        die-on-term = true 
    ```

- exemplo_flask.conf
    - esse arquivo é o de configurações do nginx, nele eu indico que o nginx deve monitorar a porta 80 e quando alguem bater na rota / ele deve mandar para o socket do uwsgi na pasta /app/exemplo_flask.sock (nome definido no arquivo app.ini)
    ```conf
        server {
            listen 80;

            location / {
                include uwsgi_params;
                uwsgi_pass unix:/app/exemplo_flask.sock;
            }

            error_page   500 502 503 504  /50x.html;
            location = /50x.html {
                root   /usr/share/nginx/html;
            }
        }
    ```

- start-service.sh
    - Esse arquivo é necessário apenas para o caso de usar o docker, pois o container roda apenas um CMD e eu preciso rodar duas coisas bloqueantes, que é o start do uwsgi e um comando para o nginx fucnionar
    ```sh
    sh -c "nohup uwsgi /app/app.ini &" && sh -c "nginx -g 'daemon off;'"
    ```
- requirements.txt
    - arquivo com as libs do python
    ```txt
        flask
        uwsgi
    ```

- docker-compose
    - apenas para build da imagem e também para montar o container com ela e espelhar a porta 80
    ```yml
    version: "3"

    services: 
        app:
            build: .
            restart: always
            ports:
                - "80:80"

    ```

Agora basta rodar o container com `docker-compose up`, esperar tudo ser criado e pronto, pode acessar `http://localhost`

